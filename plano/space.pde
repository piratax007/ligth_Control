/********************************************
Mg. Fausto Mauricio Lagos Suárez
@piratax007
2018

V1.0.0 Define los espacios y determina el 
estado de la luz en cada espacio.
********************************************/

class space {
  float[] e; 
  float[] b; 
  String label; 
  boolean state;
  color tap;
  color trans;

  space(float[] esquinas, float[] coords_button, String name) {
    e = esquinas;
    b = coords_button;
    label = name;
    state = false;
  }

  void display() {
    noStroke();
    fill(tap, trans);
    beginShape();
    for (int i = 0; i < e.length - 1; i += 2) {
      vertex(e[i], e[i + 1]);
    }
    endShape(CLOSE);
  }

  boolean update() {
    strokeWeight(6);

    if (state) {
      stroke(75);
      fill(255);
      tap = 255;
      trans = 50;
      ellipse(b[0], b[1], 36, 36);
      textAlign(LEFT, CENTER);
      text(label, b[0] + 30, b[1]);
      return true;
    } else {
      stroke(255);
      fill(75);
      tap = 0;
      trans = 180;
      ellipse(b[0], b[1], 36, 36);
      textAlign(LEFT, CENTER);
      text(label, b[0] + 30, b[1]);
      return false;
    }
  }

  void button() {
    if (dist(b[0], b[1], mouseX, mouseY) < 18) {
      state = !state;
    }
  }
  
  void switchState(){
    state = !state;
  }
}