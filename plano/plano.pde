/********************************************
Mg. Fausto Mauricio Lagos Suárez
@piratax007
2018

V1.0.0 - Control de encendido de luces desde 
el pc y por hardware. 
********************************************/

import processing.serial.*;
import cc.arduino.*;
Arduino arduino = new Arduino(this, Arduino.list()[0], 57600);

PImage apto;

float[] nodesSala = {23, 42, 453, 42, 453, 450, 23, 450};
float[] buttonSala = {1050, 550};
float[] nodesCocina = {23, 450, 453, 450, 453, 578, 420, 578, 420, 583, 423, 583, //
  423, 760, 413, 760, 413, 825, 23, 825};
float[] buttonCocina = {1050, 600};
float[] nodesH1 = {458, 42, 847, 42, 847, 450, 458, 450};
float[] buttonH1 = {1050, 650};
float[] nodesHall = {458, 453, 847, 453, 847, 578, 458, 578};
float[] buttonHall = {1050, 700};
float[] nodesWC = {505, 583, 657, 583, 657, 767, 595, 767, 595, 805, 505, 805};
float[] buttonWC = {1050, 750};
float[] nodesDucha = {662, 583, 974, 583, 974, 771, 662, 771};
float[] buttonDucha = {1050, 800};
float[] nodesH2 = {852, 293, 918, 293, 918, 42, 1175, 42, 1175, 408, 973, 408, //
  973, 578, 852, 578};
float[] buttonH2 = {1150, 550};

space sala = new space(nodesSala, buttonSala, "Sala");
space cocina = new space(nodesCocina, buttonCocina, "Cocina");
space hab1 = new space(nodesH1, buttonH1, "Hab_1");
space hall = new space(nodesHall, buttonHall, "Hall");
space wc = new space(nodesWC, buttonWC, "WC");
space ducha = new space(nodesDucha, buttonDucha, "Ducha");
space hab2 = new space(nodesH2, buttonH2, "Hab_2");

long time = 0;
long debounce = 200;

void setup() {
  size(1300, 846);
  
  apto = loadImage("apto.png");
  
  for (int i = 2; i < 9; i++) {
    arduino.pinMode(i, Arduino.OUTPUT);
  }
  
  for (int i = 9; i < 14; i++){
    arduino.pinMode(i, Arduino.INPUT);
  }
}

void draw() {
  background(0);
  image(apto, 0, 0);
  
  sala.display();
  if (sala.update()){
    arduino.digitalWrite(2, Arduino.HIGH);
  }else{
    arduino.digitalWrite(2, Arduino.LOW);
  };
  switchState(13, sala);
  
  cocina.display();
  if (cocina.update()){
    arduino.digitalWrite(3, Arduino.HIGH);
  }else{
    arduino.digitalWrite(3, Arduino.LOW);
  };
  switchState(12, cocina);
  
  hab1.display();
  if (hab1.update()){
    arduino.digitalWrite(4, Arduino.HIGH);
  }else{
    arduino.digitalWrite(4, Arduino.LOW);
  };
  switchState(11, hab1);
  
  hall.display();
  if (hall.update()){
    arduino.digitalWrite(5, Arduino.HIGH);
  }else{
    arduino.digitalWrite(5, Arduino.LOW);
  };
  switchState(10, hall);
  
  wc.display();
  if (wc.update()){
    arduino.digitalWrite(6, Arduino.HIGH);
  }else{
    arduino.digitalWrite(6, Arduino.LOW);
  };
  switchState(9, wc);
  
  ducha.display();
  if (ducha.update()){
    arduino.digitalWrite(7, Arduino.HIGH);
  }else{
    arduino.digitalWrite(7, Arduino.LOW);
  };
  switchState(8, ducha);
  
  hab2.display();
  if (hab2.update()){
    arduino.digitalWrite(8, Arduino.HIGH);
  }else{
    arduino.digitalWrite(8, Arduino.LOW);
  };
}

void mousePressed() {
  sala.button();
  cocina.button();
  hab1.button();
  hall.button();
  wc.button();
  ducha.button();
  hab2.button();
}

void switchState(int spaceSwitch, space lugar){
  if (arduino.digitalRead(spaceSwitch) == Arduino.HIGH && //
  millis() - time > debounce){
    lugar.switchState();
    time = millis();
  }
}